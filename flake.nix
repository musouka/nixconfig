{
    description = "system config flake";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

        home-manager = {
            url = "github:nix-community/home-manager/release-23.05";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        home-dirs-setup = {
            url = "./flakes/home_dirs-setup";
        };
    };

    outputs = { self, nixpkgs, home-manager, home-dirs-setup, ... }@inputs:
    let
        inherit (self) outputs;
        system = "x86_64-linux";
        pkgs = import nixpkgs {
            inherit system;

            config.allowUnfree = true;
        };
    in
    rec {
        nixosConfigurations = {
            hydrogen = nixpkgs.lib.nixosSystem {
                specialArgs = { inherit system; };
                modules = [
                    ./nixos/configuration.nix

                    home-manager.nixosModules.home-manager {
                        home-manager.useGlobalPkgs = true;
                        home-manager.useUserPackages = true;
                        home-manager.users.an = import ./users/an.nix;
                    }

                    home-dirs-setup.nixosModules.home_dirs-setup
                ];
            };
        };
    };
}
