{ lib, config, pkgs, ...}: let user = "an"; in {
    home = {
        username = user;
        homeDirectory = "/home/${user}";
        stateVersion = "23.05";
        activation.xdr_setup = lib.hm.dag.entryAfter [ "writeBoundary" "onFilesChange" ] ''
        ${lib.getExe pkgs.home_dirs-setup}
        '';

    };

    programs.home-manager.enable = true;
    fonts.fontconfig.enable = true;
}
