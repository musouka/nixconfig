{
  description = "Configuration to setup xdr-dirs";

  outputs = { self, nixpkgs }:
    let

    version = "0.0.9";
    supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; overlays = [ self.overlay ]; });
    in
    {
        overlay = final: prev: {
            home_dirs-setup = with final; stdenv.mkDerivation rec {
                name = "home_dirs-setup-${version}";
                dontUnpack = true;
                buildInputs = with final; [xdg-user-dirs];
                buildPhase = let bin_path = "${xdg-user-dirs}/bin/xdg-user-dirs-update"; in ''
                    cat > home_dirs-setup <<EOF
                    #! $SHELL
                    ${bin_path} --set DESKTOP      ~/desktop
                    ${bin_path} --set DOCUMENTS    ~/documents
                    ${bin_path} --set DOWNLOAD     ~/downloads
                    ${bin_path} --set MUSIC        ~/music
                    ${bin_path} --set PICTURES     ~/pictures
                    ${bin_path} --set PUBLICSHARE  ~/public
                    ${bin_path} --set TEMPLATES    ~/templates
                    ${bin_path} --set VIDEOS       ~/videos
                    EOF
                    chmod +x home_dirs-setup
                '';

                installPhase =''
                    mkdir -p $out/bin
                    cp home_dirs-setup $out/bin/
                '';
                meta.mainProgram = "home_dirs-setup";
            };
        };

        packages = forAllSystems (system:
        {
          inherit (nixpkgsFor.${system}) home_dirs-setup;
        });

        nixosModules.home_dirs-setup =
        { pkgs, ... }:
        {
            nixpkgs.overlays = [ self.overlay ];
            environment.systemPackages = [ pkgs.home_dirs-setup ];
        };
    };
}
